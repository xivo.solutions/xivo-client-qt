<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>BaseEngine</name>
    <message>
        <location filename="src/baseengine.cpp" line="515"/>
        <source>Connection lost with XiVO CTI server</source>
        <translation>Verbindung mit dem XiVO CTI-Server verloren</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="776"/>
        <source>Could not modify the Services data.</source>
        <translation>Die Dienstdaten konnten nicht geändert werden.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="776"/>
        <source>Maybe Asterisk is down.</source>
        <translation>Vielleicht ist Asterisk außer Betrieb.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1113"/>
        <source>Failed to start a secure connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1114"/>
        <source>Do you want to disable secure connections?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1138"/>
        <source>Server has sent an Error.</source>
        <translation>Server hat einen Fehler gesendet.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1144"/>
        <source>Your registration name &lt;%1@%2&gt; is not known by the XiVO CTI server on %3:%4.</source>
        <translation>Ihr Registrierungsname &lt;%1@%2&gt; ist dem XiVO CTI-Server auf %3:%4 nicht bekannt.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1149"/>
        <source>You entered a wrong login / password.</source>
        <translation>Sie haben einen falschen Benutzernamen / Passwort eingegeben.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1152"/>
        <source>You have no profile defined.</source>
        <translation>Sie haben kein Profil angegeben.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1156"/>
        <source>The server %1:%2 did not reply to the last keepalive packet.</source>
        <oldsource>The XiVO CTI server on %1:%2 did not reply to the last keepalive.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1162"/>
        <source>You defined an IP address %1 that is probably an unresolved host name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1166"/>
        <source>Socket timeout (~ 60 s) : you probably attempted to reach, via a gateway, an IP address %1 that does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1171"/>
        <source>There seems to be a machine running on this IP address %1, and either no CTI server is running, or your port %2 is wrong.</source>
        <translation>Es scheint, dass eine Maschine auf der IP-Adresse %1 läuft und entweder kein CTI-Server läuft oder Ihr Port %2 falsch ist.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1176"/>
        <source>An error occurred on the network while attempting to join the IP address %1 :
- no external route defined to access this IP address (~ no timeout)
- this IP address is routed but there is no machine (~ 5 s timeout)
- a cable has been unplugged on your LAN on the way to this IP address (~ 30 s timeout).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1183"/>
        <source>It seems that the server with IP address %1 does not accept encryption on its port %2. Please change either your port or your encryption setting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1188"/>
        <source>An unknown socket error has occured while attempting to join the IP address:port %1:%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1193"/>
        <source>An unmanaged (number %1) socket error has occured while attempting to join the IP address:port %1:%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1198"/>
        <source>The XiVO CTI server on %1:%2 has just closed the connection.</source>
        <translation>Der XiVO CTI-Server auf %1:%2 hat gerade die Verbindung geschlossen.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1202"/>
        <source>The XiVO CTI server on %1:%2 has just been stopped.</source>
        <translation>Der XiVO CTI-Server auf %1:%2 wurde gerade gestoppt.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1205"/>
        <source>The XiVO CTI server on %1:%2 has just been reloaded.</source>
        <translation>Der XiVO CTI-Server auf %1:%2 wurde gerade neu geladen.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1209"/>
        <source>You are already connected to %1:%2.</source>
        <oldsource>You are already connected from %1:%2.</oldsource>
        <translation>Sie sind bereits mit %1:%2 verbunden.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1211"/>
        <source>No capability allowed.</source>
        <translation>Keine Möglichkeit erlaubt.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1214"/>
        <source>Max number (%1) of XiVO Clients already reached.</source>
        <translation>Maximale Anzahl (%1) an XiVO-Clienten bereits erreicht.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1216"/>
        <source>Missing Argument(s)</source>
        <translation>Fehlende(s) Argument(e)</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1221"/>
        <source>Your client&apos;s protocol version (%1)
is not the same as the server&apos;s (%2).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1229"/>
        <source>Your server version (%1) is too old for this client.
Please upgrade it to %2 at least.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1234"/>
        <source>Your server version (%1) is too old for this client.
Please upgrade it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1238"/>
        <source>You were disconnected by the server.</source>
        <translation>Sie wurden vom Server getrennt.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1240"/>
        <source>You were forced to disconnect by the server.</source>
        <translation>Sie wurden vom Server zur Trennung gezwungen.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1242"/>
        <source>Could not log agent: invalid extension.</source>
        <oldsource>Invalid extension number</oldsource>
        <translation>Ungültige Nebenstellennummer</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1244"/>
        <source>Could not log agent: extension already in use.</source>
        <translation>Agent konnte nicht angemeldet werden: Nebenstelle bereits in Gebrauch.</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1247"/>
        <source>Unreachable number: %1</source>
        <translation>Unerreichbare Nummer: %1</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1249"/>
        <source>The authentication server could not fulfill your request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1258"/>
        <source>ERROR</source>
        <translation>FEHLER</translation>
    </message>
    <message>
        <location filename="src/baseengine.cpp" line="1414"/>
        <source>Attempting to reconnect to server</source>
        <translation>Versuch der Wiederverbindung mit dem Server</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="src/storage/queuememberinfo.cpp" line="86"/>
        <source>Agent</source>
        <translation>Agent</translation>
    </message>
    <message>
        <location filename="src/storage/queuememberinfo.cpp" line="86"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
</context>
<context>
    <name>QueueAgentStatus</name>
    <message>
        <location filename="src/storage/queue_agent_status.cpp" line="51"/>
        <source>Dynamic membership</source>
        <translation>Dynamische Mitgliedschaft</translation>
    </message>
    <message>
        <location filename="src/storage/queue_agent_status.cpp" line="55"/>
        <source>Static/RT membership</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="src/storage/queue_agent_status.cpp" line="67"/>
        <source>Agent not in Queue</source>
        <translation>Agent nicht in Warteschlange</translation>
    </message>
    <message>
        <location filename="src/storage/queue_agent_status.cpp" line="71"/>
        <location filename="src/storage/queue_agent_status.cpp" line="83"/>
        <source>Agent in Queue</source>
        <translation>Agent in Warteschlange</translation>
    </message>
    <message>
        <location filename="src/storage/queue_agent_status.cpp" line="72"/>
        <location filename="src/storage/queue_agent_status.cpp" line="76"/>
        <source>Logged</source>
        <oldsource>Logged in</oldsource>
        <translation>Angemeldet</translation>
    </message>
    <message>
        <location filename="src/storage/queue_agent_status.cpp" line="75"/>
        <source>Agent Called or Busy</source>
        <translation>Agent angerufen oder besetzt</translation>
    </message>
    <message>
        <location filename="src/storage/queue_agent_status.cpp" line="79"/>
        <source>Agent in Queue but Invalid</source>
        <translation>Agent in Warteschlange, aber ungültig</translation>
    </message>
    <message>
        <location filename="src/storage/queue_agent_status.cpp" line="84"/>
        <source>Unlogged</source>
        <oldsource>Logged out</oldsource>
        <translation>Abgemeldet</translation>
    </message>
    <message>
        <location filename="src/storage/queue_agent_status.cpp" line="92"/>
        <source>Unpaused</source>
        <oldsource>Not paused</oldsource>
        <translation>Nicht pausiert</translation>
    </message>
    <message>
        <location filename="src/storage/queue_agent_status.cpp" line="96"/>
        <source>Paused</source>
        <translation>Pausiert</translation>
    </message>
    <message>
        <location filename="src/storage/queue_agent_status.cpp" line="100"/>
        <source>Not relevant</source>
        <translation>Nicht relevant</translation>
    </message>
</context>
</TS>
