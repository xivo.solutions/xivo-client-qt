#!/bin/bash
set -x
WORKING_DIR='xivo-client-qt'

cleanup () {
    if [ -d "$WORKING_DIR" ]; then rm -rf $WORKING_DIR; fi
}

download () {
    git clone https://gitlab.com/xivo.solutions/xivo-client-qt.git
    cd xivo-client-qt
}

build () {
    source build-deps
    export PATH=$WIN_QT_PATH/bin:$WIN_MINGW_PATH/bin:$PATH
    qmake
    mingw32-make SHELL=
    mingw32-make pack
}

echo XiVO Client Install for Windows
cleanup
download
build
